$(document).on('ready', function() {
    $(".slider").slick({
      dots: true, 
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    }); 
  });